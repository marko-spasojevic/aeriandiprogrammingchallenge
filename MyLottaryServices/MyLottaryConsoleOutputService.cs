﻿using LotteryServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLottaryServices
{
    public class MyLottaryConsoleOutputService: ILottaryOutputService
    {
        private readonly ILotteryGenaratorService _lotteryGenaratorService;

        public MyLottaryConsoleOutputService(ILotteryGenaratorService lotteryGenaratorService)
        {
            _lotteryGenaratorService = lotteryGenaratorService;
        }
        public void PrintLottaryBallNumbers(int min, int max, int ballNumbers)
        {
            HashSet<int> numbers = _lotteryGenaratorService.GenerateRandomNumbers(min, max, ballNumbers);
            Console.ForegroundColor = ConsoleColor.White;
            if (numbers != null && numbers.Count > 0)
            {
                FormatConsoleOutput(numbers);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Press any key to exit program");
                Console.ReadLine();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There is generated numbers !!!!");
                Console.ReadKey();
            }
        }

        private void FormatConsoleOutput(HashSet<int> numbers)
        {
            Console.Write("{");
            var lastNumber = numbers.Last();
            foreach (var number in numbers)
            {
                Console.ForegroundColor = GetBackgroundColorBasedOnNumber(number);
                Console.Write(number);
                if (lastNumber != number)
                    Console.Write(",");
            }
            Console.WriteLine("}");
        }

        private ConsoleColor GetBackgroundColorBasedOnNumber(int number)
        {
            if (number >= 1 && number <= 9) return ConsoleColor.Gray;

            if (number >= 10 && number <= 19) return ConsoleColor.Blue;

            if (number >= 20 && number <= 29) return ConsoleColor.Red; //there is no pink I put Red insted

            if (number >= 30 && number <= 39) return ConsoleColor.Green;

            if (number >= 40 && number <= 49) return ConsoleColor.Yellow;

            return ConsoleColor.White;
        }
    }
}
