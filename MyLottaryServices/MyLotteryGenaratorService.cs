﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotteryServiceInterfaces;

namespace MyLottaryServices
{
    public class MyLotteryGenaratorService: ILotteryGenaratorService
    {
        public HashSet<int> GenerateRandomNumbers(int minValue, int maxValue, int numberOfBalls)
        {
            HashSet<int> numbers = new HashSet<int>();
            Random randomNum = new Random();
            while (numbers.Count < numberOfBalls)
            {
                numbers.Add(randomNum.Next(minValue, maxValue));
            }

            return numbers;
        }
    }
}
