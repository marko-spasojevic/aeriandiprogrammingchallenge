﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotteryServiceInterfaces
{
    public interface ILotteryGenaratorService
    {
        HashSet<int> GenerateRandomNumbers(int minValue, int maxValue, int numberOfBalls);
    }
}
