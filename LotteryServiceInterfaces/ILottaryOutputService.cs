﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotteryServiceInterfaces
{
    public interface ILottaryOutputService
    {
        void PrintLottaryBallNumbers(int minValue, int maxValue, int ballNumbers);
    }
}
