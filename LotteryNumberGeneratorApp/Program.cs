﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Autofac;
using LotteryServiceInterfaces;
using MyLottaryServices;

namespace LotteryNumberGeneratorApp
{
    class Program
    {
        private static IContainer Container { get; set; }

        public static void generateNumbers()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var generator = scope.Resolve<ILottaryOutputService>();

                Console.WriteLine("Enter number of balls(default one is 6):");
                string enteredValue = Console.ReadLine();
                int ballsNumUI = enteredValue != null && Regex.IsMatch(enteredValue, @"\d") ? Convert.ToInt32(enteredValue) : 6;

                int ballNum = ballsNumUI == 0 ? 6 : ballsNumUI;
                generator.PrintLottaryBallNumbers(1, 39, ballNum);
            }
        }
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<MyLotteryGenaratorService>().As<ILotteryGenaratorService>();
            builder.RegisterType<MyLottaryConsoleOutputService>().As<ILottaryOutputService>();
            Container = builder.Build();
            generateNumbers();
        }
    }
}
